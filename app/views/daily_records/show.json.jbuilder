# frozen_string_literal: true

json.partial! 'daily_records/daily_record', daily_record: @daily_record
