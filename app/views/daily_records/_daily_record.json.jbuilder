# frozen_string_literal: true

json.extract! daily_record, :id, :note, :date, :created_at, :updated_at
json.url daily_record_url(daily_record, format: :json)
