# frozen_string_literal: true

# app/models/daily_record.rb
class DailyRecord < ApplicationRecord
  include Rails.application.routes.url_helpers

  has_one_attached :image
  belongs_to :user

  def image_url
    image.attached? ? url_for(image) : ''
  end

  def self.filter(opts)
    result = if opts[:id]
               DailyRecord.find(opts[:id])
             else
               res = DailyRecord.page(opts[:page]).per(10).all
               res.where(opts[:query]) if opts[:query].present?
               res
             end
    DailyRecordSerializer.new(result)
  end
end
