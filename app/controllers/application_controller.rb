# frozen_string_literal: true

# app/controllers/application_controller.rb
class ApplicationController < ActionController::Base
  # before_action :check_request_agent_type

  private

  def check_request_agent_type
    return true if request.user_agent =~ /android|blackberry|iphone|ipod|iemobile|mobile|webos/

    render json: { message: 'Please send request Via mobile devise' }
  end
end
