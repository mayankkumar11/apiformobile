# frozen_string_literal: true

module Users
  # app/controllers/users/social_auth_controller.rb
  class SocialAuthController < ApplicationController
    skip_before_action :verify_authenticity_token

    def authenticate_social_auth_user
      @user = User.signin_or_create_from_provider(params)
      if @user.persisted?
        sign_in(@user)

        response.headers['Authorization'] = "Bearer #{@user.create_new_auth_token}"
        message = 'User was successfully logged in through Facebook'
        construct_response(:created, message, UserSerializer.new(@user).serializable_hash)
      else
        message = 'There was a problem signing you in through Facebook'
        construct_response(:unprocessable_entity, message, data: @user.errors.full_messages)
      end
    end

    private

    def construct_response(status, message, payload)
      render json: {
        message: message
      }.merge(payload), status: status
    end
  end
end
