# frozen_string_literal: true

module Users
  # app/controllers/users/registrations_controller.rb
  class SessionsController < Devise::SessionsController
    skip_before_action :verify_authenticity_token

    respond_to :json

    private

    def respond_with(resource, _opts = {})
      render json: {
        status: { code: 200, message: 'Logged in successfully.' },
        data: UserSerializer.new(resource).serializable_hash[:data][:attributes]
      }
    end

    def respond_to_on_destroy
      head :ok
    end
  end
end
