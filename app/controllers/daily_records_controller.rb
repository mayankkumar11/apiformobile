# frozen_string_literal: true

# app/controllers/daily_records_controller.rb
class DailyRecordsController < ApplicationController
  before_action :set_daily_record, only: %i[edit update destroy]
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!

  def index
    @daily_records = DailyRecord.filter(params)
    render json: { daily_records: @daily_records.serializable_hash }, status: 200
  end

  def show
    @daily_record = DailyRecord.filter(params)
    render json: { daily_records: @daily_record.serializable_hash }, status: 200
  end

  def new
    @daily_record = DailyRecord.new
  end

  def edit; end

  def create
    @daily_record = DailyRecord.new(daily_record_params)

    respond_to do |format|
      if @daily_record.save
        format.json { render :show, format: :json }
      else
        format.json { render json: @daily_record.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @daily_record.update(daily_record_params)
        format.json { render :show, format: :json }
      else
        format.json { render json: @daily_record.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @daily_record = DailyRecord.filter(params).destroy_record
    render json: { message: 'Daily record was successfully destroyed.', data: @daily_record }
  end

  private

  def set_daily_record
    @daily_record = DailyRecord.find(params[:id])
  end

  def daily_record_params
    params.require(:daily_record).permit(:note, :date, :user_id, :image)
  end
end
