# frozen_string_literal: true

# app/controllers/users_controller.rb
class UsersController < ApplicationController
  def profile
    query = { query: { user: current_user.id } }
    daily_records = DailyRecord.filter(query) if current_user.daily_records
    render json: { daily_records: daily_records }, status: 200
  end
end
