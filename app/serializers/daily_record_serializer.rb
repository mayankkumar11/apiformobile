# frozen_string_literal: true

# app/serializers/daily_record_serializer.rb
class DailyRecordSerializer
  attr_accessor :resource

  include FastJsonapi::ObjectSerializer
  attributes :id, :note, :date, :created_at, :updated_at

  attribute :image_url do |object|
    object&.image_url
  end

  def destroy_record
    resource.destroy
    serializable_hash
  end
end
