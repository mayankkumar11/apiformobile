# frozen_string_literal: true

Rails.application.routes.draw do
  default_url_options host: 'localhost:3000/'

  devise_for :users, path: '', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    registration: 'signup'
  }, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  resources :daily_records
  get 'profile', to: 'users#profile'
  post 'social_auth/callback', to: 'users/social_auth#authenticate_social_auth_user'
end
