# frozen_string_literal: true

# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

ENV['FACEBOOK_APP_ID'] = 'YOUR_FACEBOOK_APP_ID'
ENV['FACEBOOK_APP_SECRET'] = 'YOUR_FACEBOOK_APP_SECRET'
