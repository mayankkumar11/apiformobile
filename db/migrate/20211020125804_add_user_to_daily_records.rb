# frozen_string_literal: true

# db/migrate/20211020125804_add_user_to_daily_records.rb
class AddUserToDailyRecords < ActiveRecord::Migration[6.1]
  def change
    add_reference :daily_records, :user, foreign_key: true
  end
end
