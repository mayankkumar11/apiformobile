# frozen_string_literal: true

# db/migrate/20211021124623_add_omniauth_to_users.rb
class AddOmniauthToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
  end
end
