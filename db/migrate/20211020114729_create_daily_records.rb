# frozen_string_literal: true

# db/migrate/20211020114729_create_daily_records.rb
class CreateDailyRecords < ActiveRecord::Migration[6.1]
  def change
    create_table :daily_records do |t|
      t.string :image
      t.text :note
      t.date :date

      t.timestamps
    end
  end
end
