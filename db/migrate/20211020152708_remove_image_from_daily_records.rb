# frozen_string_literal: true

# db/migrate/20211020152708_remove_image_from_daily_records.rb
class RemoveImageFromDailyRecords < ActiveRecord::Migration[6.1]
  def change
    remove_column :daily_records, :image, :string
  end
end
