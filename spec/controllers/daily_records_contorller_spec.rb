# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DailyRecordsController, type: :controller do
  let!(:user) { User.create(email: "user#{rand(1..1000)}@example.com", password: '123456', password_confirmation: '123456') }
  let!(:valid_attributes) { { note: 'aaaaa', date: Date.new, user_id: user.id } }
  let!(:invalid_attributes) { { note: 'aa', date: Date.new, user_id: user.id } }
  let!(:new_attributes) { { note: 'aaaaa2', date: Date.new, user_id: user.id } }

  describe 'GET #index' do
    xit 'render :index' do
      get :index
      expect render_template(:index)
    end

    xit 'should respond with 200' do
      get :index
      expect response.status == 200
    end
  end

  describe 'GET #show' do
    xit 'returns a success response' do
      daily_record = DailyRecord.create! valid_attributes
      get :show, params: { id: daily_record.to_param }
      expect(response).to be_successful
    end
  end

  describe '#new' do
    let(:action_response) { get(:new) }
    subject { action_response }

    before do
      action_response
    end

    context 'daily_record' do
      xit { should_not.nil? }
    end
  end

  describe 'GET #edit' do
    xit 'returns a success response' do
      daily_record = DailyRecord.create! valid_attributes
      get :edit, params: { id: daily_record.to_param }
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      xit 'creates a new daily_record' do
        expect do
          post :create, params: { daily_record: valid_attributes }
        end.to change(daily_record, :count).by(1)
      end

      xit 'redirects to the created daily_record' do
        post :create, params: { daily_record: valid_attributes }
        expect(response).to redirect_to(daily_record.last)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      xit 'updates the requested daily_record' do
        daily_record = DailyRecord.create! valid_attributes
        put :update, params: { id: daily_record.to_param, daily_record: new_attributes }
        daily_record.reload
      end

      xit 'redirects to the daily_record' do
        daily_record = DailyRecord.create! valid_attributes
        put :update, params: { id: daily_record.to_param, daily_record: valid_attributes }
        expect(response).to redirect_to(daily_record)
      end
    end

    context 'with invalid params' do
      xit "returns a success response (i.e. to display the 'edit' template)" do
        daily_record = DailyRecord.create! valid_attributes
        put :update, params: { id: daily_record.to_param, daily_record: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    xit 'destroys the requested daily_record' do
      daily_record = DailyRecord.create! valid_attributes
      expect do
        delete :destroy, params: { id: daily_record.to_param }
      end.to change(daily_record, :count).by(-1)
    end

    xit 'redirects to the daily_records list' do
      daily_record = DailyRecord.create! valid_attributes
      delete :destroy, params: { id: daily_record.to_param }
      expect(response).to redirect_to(daily_records_url)
    end
  end
end
