# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DailyRecord, type: :model do
  describe 'fields' do
    it { should have_db_column(:note).of_type(:text) }
    it { should have_db_column(:date).of_type(:date) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
    it { should have_db_column(:created_at).of_type(:datetime) }
  end

  describe 'associations' do
    it { should belong_to(:user) }
  end
end
